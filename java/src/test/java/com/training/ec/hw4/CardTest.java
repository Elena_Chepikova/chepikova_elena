package com.training.ec.hw4;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {

  @Test
  public void testGetCardholder() {
    Card card = new Card ("ELENA CHEPIKOVA");
    String expected = "ELENA CHEPIKOVA";
    Assert.assertEquals(expected,card.getCardholder());
  }

  @Test
  public void testGetBalance1() {
    Card card = new Card ("ELENA CHEPIKOVA");
    Assert.assertEquals(0.0,card.getBalance(),0);
  }

  @Test
  public void testGetBalance2() {
    Card card = new Card ("ELENA CHEPIKOVA", 666);
    Assert.assertEquals(666.00,card.getBalance(),0);
  }

  @Test
  public void testDeposit1() {
    Card card = new Card ("ELENA CHEPIKOVA");
    double currBalance = card.deposit(100);
    Assert.assertEquals(100,currBalance,0);
  }

  @Test
  public void testDeposit2() {
    Card card = new Card ("ELENA CHEPIKOVA", 500);
    double currBalance = card.deposit(100);
    Assert.assertEquals(600,currBalance,0);
  }

  @Test
  public void testWithdraw() {
    Card card = new Card ("ELENA CHEPIKOVA", 500);
    double currBalance = card.withdraw(100);
    Assert.assertEquals(400,currBalance,0);
  }

  @Test
  public void testExchange() {
    Card card = new Card ("ELENA CHEPIKOVA", 500);
    double currBalance = card.exchange(2);
    Assert.assertEquals(1000.00,currBalance,0);
  }
}