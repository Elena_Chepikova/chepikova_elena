package com.training.ec.hw5;

import com.training.ec.hw4.Card;
import org.junit.Assert;
import org.junit.Test;

public class TestAtm {

  @Test
  public void testShowBalance1() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA");
    Atm atm = new Atm(debitCard);
    Assert.assertEquals(0.0, atm.showBalance(), 0);
  }

  @Test
  public void testShowBalance2() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 100500.55);
    Atm atm = new Atm(debitCard);
    Assert.assertEquals(100500.55, atm.showBalance(), 0);
  }

  @Test
  public void testGiveMoney1() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 100);
    Atm atm = new Atm(debitCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(90));
  }

  @Test
  public void testGiveMoney2() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 100);
    Atm atm = new Atm(debitCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(100));
  }

  @Test
  public void testGiveMoney3() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 100);
    Atm atm = new Atm(debitCard);
    String expected = "No money - no honey";
    Assert.assertEquals(expected, atm.giveMoney(200));
  }

  @Test
  public void testGiveMoney4() {
    CreditCard creditCard = new CreditCard("ELENA CHEPIKOVA", 100);
    Atm atm = new Atm(creditCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(90));
  }

  @Test
  public void testGiveMoney5() {
    CreditCard creditCard = new CreditCard("ELENA CHEPIKOVA", 100);
    Atm atm = new Atm(creditCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(200));
  }

  @Test
  public void testReceiveMoney1() {
    CreditCard creditCard = new CreditCard("ELENA CHEPIKOVA");
    Atm atm = new Atm(creditCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(200));
  }

  @Test
  public void testReceiveMoney2() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 300);
    Atm atm = new Atm(debitCard);
    String expected = "OK";
    Assert.assertEquals(expected, atm.giveMoney(200));
  }

  @Test
  public void testShowBalance3() {
    DebitCard debitCard = new DebitCard("ELENA CHEPIKOVA", 1000);
    Atm atm = new Atm(debitCard);
    atm.giveMoney(900);
    atm.receiveMoney(500);
    Assert.assertEquals(600, atm.showBalance(), 0);
  }
}
