package com.training.ec.hw5_2;

public class Program {
  public static void main(String[] args) {
    int[] arr = new int[] {37, 2, 25, 4, 18};
    SortMethod method = new SortMethod(new SelectionSort());
    method.execute(arr);
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }

    System.out.println();

    method.setSort(new BubbleSort());
    method.execute(arr);
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }
  }
}
