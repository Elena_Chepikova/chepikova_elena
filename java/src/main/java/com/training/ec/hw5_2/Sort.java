package com.training.ec.hw5_2;

public interface Sort {
  void sort(int[] arr);
}
