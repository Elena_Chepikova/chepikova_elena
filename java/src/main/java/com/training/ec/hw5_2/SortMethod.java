package com.training.ec.hw5_2;

public class SortMethod {
  private Sort sort;

  public void setSort(Sort sort) {
    this.sort = sort;
  }

  public SortMethod(Sort sort) {
    this.sort = sort;
  }

  public void execute(int[] arr) {
    sort.sort(arr);
  }
}
