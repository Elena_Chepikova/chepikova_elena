package com.training.ec.hw4;

import java.util.Arrays;

public final class Median {

  public static float median(int[] arr) {
    float result;

    Arrays.sort(arr);

    if (arr.length % 2 == 0) {
      result = ((float) arr[(arr.length - 1) / 2] + arr[(arr.length - 1) / 2 + 1]) / 2;
    } else {
      result = arr[(arr.length) / 2];
    }
    return result;
  }

  public static double median(double[] arr) {
    double result;

    Arrays.sort(arr);

    if (arr.length % 2 == 0) {
      result = (arr[(arr.length - 1) / 2] + arr[(arr.length - 1) / 2 + 1]) / 2;
    } else {
      result = arr[(arr.length) / 2];
    }
    return result;
  }
}