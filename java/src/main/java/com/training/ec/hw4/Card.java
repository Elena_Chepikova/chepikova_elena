package com.training.ec.hw4;

public class Card {
  public String cardholder;
  public double currBalance;

  public Card(String cardholder) {
    this.cardholder = cardholder;
  }

  public Card(String cardholder, double currBalance) {
    this(cardholder);
    this.currBalance = currBalance;
  }

  public double deposit(double amount) {
    currBalance += amount;
    return currBalance;
  }

  public double withdraw(double amount) {
    currBalance -= amount;
    return currBalance;
  }

  public double exchange(double rate) {
    currBalance *= rate;
    return currBalance;
  }

  public double getBalance() {
    return currBalance;
  }

  public String getCardholder() {
    return cardholder;
  }
}
