package com.training.ec.hw5;

import com.training.ec.hw4.Card;

public class CreditCard extends Card {
  public CreditCard(String cardholder) {
    super(cardholder);
  }

  public CreditCard(String cardholder, double currBalance) {
    super(cardholder, currBalance);
  }
}
