package com.training.ec.hw5;

import com.training.ec.hw4.Card;

public class Atm {
  private Card card;

  public Atm(Card card) {
    this.card = card;
  }

  public String giveMoney(double amount) {
    try {
      card.withdraw(amount);
    } catch (NegativeBalanceException e) {
      return "No money - no honey";
    }
    return "OK";
  }

  public String receiveMoney(double amount) {
    card.deposit(amount);
    return "OK";
  }

  public double showBalance() {
    return card.getBalance();
  }
}
