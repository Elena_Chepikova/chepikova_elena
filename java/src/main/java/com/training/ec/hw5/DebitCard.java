package com.training.ec.hw5;

import com.training.ec.hw4.Card;

public class DebitCard extends Card {
  public DebitCard(String cardholder) {
    super(cardholder);
  }

  public DebitCard(String cardholder, double currBalance) {
    super(cardholder, currBalance);
  }

  @Override
  public double withdraw(double amount) throws NegativeBalanceException {
    if (currBalance < amount) {
      throw new NegativeBalanceException("Negative balance");
    }
    currBalance -= amount;
    return currBalance;
  }
}
