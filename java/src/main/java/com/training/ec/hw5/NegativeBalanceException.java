package com.training.ec.hw5;

public class NegativeBalanceException extends RuntimeException {
  public NegativeBalanceException(String message) {
    super(message);
  }
}
