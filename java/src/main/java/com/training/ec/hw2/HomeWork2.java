package com.training.ec.hw2;

public class HomeWork2 {
  public static void main(String[] args) {
    int algorithmId = Integer.parseInt(args[0]);
    int loopType = Integer.parseInt(args[1]);
    int n = Integer.parseInt(args[2]);
    int i = 3;
    int f1 = 1;
    int f2 = 1;
    int f;

    if ((algorithmId == 1) && (loopType == 1)) {
      if (n == 1) {
        System.out.println("1");
      } else if (n == 2) {
        System.out.println("1 1");
      } else {
        System.out.print("1 1 ");
        while (i <= n) {
          f = f1 + f2;
          f1 = f2;
          f2 = f;
          i++;
          System.out.print(f + " ");
        }
      }
    }

    if ((algorithmId == 1) && (loopType == 2)) {
      if (n == 1) {
        System.out.println("1");
      } else if (n == 2) {
        System.out.println("1 1");
      } else {
        System.out.print("1 1 ");
        do {
          f = f1 + f2;
          f1 = f2;
          f2 = f;
          i++;
          System.out.print(f + " ");
        } while (i <= n);
      }
    }

    if ((algorithmId == 1) && (loopType == 3)) {
      if (n == 1) {
        System.out.println("1");
      } else if (n == 2) {
        System.out.println("1 1");
      } else {
        System.out.print("1 1 ");
        for (i = 3; i <= n; i++) {
          f = f1 + f2;
          f1 = f2;
          f2 = f;
          i++;
          System.out.print(f + " ");
        }
      }
    }

    if ((algorithmId == 2) && (loopType == 1)) {
      i = 1;
      f = 1;
      while (i <= n) {
        f = f * i;
        i++;
      }
      System.out.println(f);
    }

    if ((algorithmId == 2) && (loopType == 2)) {
      i = 1;
      f = 1;
      do {
        f = f * i;
        i++;
      } while (i <= n);
      System.out.println(f);
    }

    if ((algorithmId == 2) && (loopType == 3)) {
      f = 1;
      for (i = 1; i <= n; i++) {
        f = f * i;
      }
      System.out.println(f);
    }
    else {
      System.out.println("Invalid arguments are used!");
    }
  }
}
