package com.training.ec.hw3;

import java.sql.Array;

public class Initializer {

  private int int0;
  private byte byte0;
  private short short0;
  private long long0;
  private float float0;
  private double double0;
  private char char0;
  private boolean boolean0;
  private Array array0;
  private String string0;
  private Class class0;

  public int getInt0() {
    return int0;
  }

  public byte getByte0() {
    return byte0;
  }

  public short getShort0() {
    return short0;
  }

  public long getLong0() {
    return long0;
  }

  public float getFloat0() {
    return float0;
  }

  public double getDouble0() {
    return double0;
  }

  public char getChar0() {
    return char0;
  }

  public boolean isBoolean0() {
    return boolean0;
  }

  public Array getArray0() {
    return array0;
  }

  public String getString0() {
    return string0;
  }

  public Class getClass0() {
    return class0;
  }

  public static void main(String[] args) {
    Initializer init = new Initializer();
    System.out.println("int = " + init.getInt0());
    System.out.println("byte = " + init.getByte0());
    System.out.println("short = " + init.getShort0());
    System.out.println("long = " + init.getLong0());
    System.out.println("float = " + init.getFloat0());
    System.out.println("double = " + init.getDouble0());
    System.out.println("char = " + init.getChar0());
    System.out.println("boolean = " + init.isBoolean0());
    System.out.println("Array = " + init.getArray0());
    System.out.println("String = " + init.getString0());
    System.out.println("Class = " + init.getClass0());
  }
}

