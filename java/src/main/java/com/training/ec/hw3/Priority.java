package com.training.ec.hw3;

public class Priority {
  public static int a;
  public int b;

  {
    b = 5;
    System.out.println(b + " - Not static variable 1");
    System.out.println("6 - Not static block 1");
  }

  {
    b = 7;
    System.out.println(b + " - Not static variable 2");
    System.out.println("8 - Not static block 2");
  }

  static {
    a = 1;
    System.out.println(a + " - Static variable 1");
    System.out.println("2 - Static block 1");
  }

  static {
    a = 3;
    System.out.println(3 + " - Static variable 2");
    System.out.println("4 - Static block 2");
  }

  public static void main (String[] args) {
    String c;
    c = "9 - Reference variable in method main";
    Priority abc = new Priority();
    a=10;
    abc.b=11;

    System.out.println(c);
    System.out.println(a + " - Other (static variable example)");
    System.out.println(abc.b + " - Other (variable example)");
  }
}
